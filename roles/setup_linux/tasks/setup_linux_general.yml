---

- name: create/validate oracle groups and user
  include_tasks: tasks/oracle_groups_and_user.yml

- name: install xfs filesystem
  yum:
    name:
      - xfsprogs
      - xfsdump
    state: installed
  when: 
    - ansible_distribution_major_version|int == 6 
    - filesystem == "xfs"

- name: additional packages that might have been forgotten in the preinstall rpm
  yum:
    name:
      - perl
      - unzip
      - unixODBC
      - acl
      - rsync
    state: installed

- name: set selinux to enforcing mode, selinux enabled
  selinux:
    policy: targeted
    state: enforcing
  when: selinux == "Y"

- name: set selinux to permissive mode, selinux enabled
  selinux:
    policy: targeted
    state: permissive
  when: selinux == "N"

- name: set /etc/hosts remove 127.0.0.1 for hostname
  lineinfile:
    dest: /etc/hosts
    regexp: '^127\.0\.0\.1.*{{ ansible_hostname }}'
    state: absent

- name: set /etc/hosts set localonly ip address for hostname
  lineinfile:
    dest: /etc/hosts
    regexp: '^{{ host_set_ip_address }}'
    line: '{{ host_set_ip_address }} {{ ansible_fqdn }} {{ ansible_hostname }}'
    state: present

- name: disable removal of oracle sockets by systemd-tmpfiles-clean.service
  blockinfile:
    path: /usr/lib/tmpfiles.d/tmp.conf
    block: |
      x /tmp/.oracle*
      x /var/tmp/.oracle*
      x /usr/tmp/.oracle*
  when: ansible_service_mgr == 'systemd'

- name: get grub entries that do not have transparent hugepages disabled
  shell: >
    grubby --info=ALL | grep args | grep -v transparent_hugepage=never || true
  register: non_thp_entries
  changed_when: non_thp_entries.stdout | count | int > 0
  notify: disable transparent hugepages for all grub kernel entries

- name: verify runtime transparent hugepage settings
  shell: >
    for FILE in /sys/kernel/mm/transparent_hugepage/enabled /sys/kernel/mm/transparent_hugepage/defrag; do grep -v '\[never\]' $FILE; done || true
  register: runtime_thp
  changed_when: runtime_thp.stdout | count | int > 0
  notify: disable runtime transparent hugepages
