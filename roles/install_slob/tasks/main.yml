---

- name: install/validate git and the gcc compiler packages
  yum:
    name: 
      - git
      - gcc
    state: installed
  become: true
  become_user: root

- name: clone slob repository
  git:
    repo: https://github.com/therealkevinc/SLOB_distribution
    dest: "{{ slob_directory }}"
    version: 2.5.2.4

- name: obtain name of the slob tarball
  shell: >
    ls {{ slob_directory }}/*slob*tar.gz
  register: tarball_name
  changed_when: false

- name: untar slob tarball
  unarchive:
    src: "{{ tarball_name.stdout }}"
    dest: "{{ slob_directory }}"
    creates: "{{ slob_directory }}/SLOB/slob.sql"
    remote_src: yes

- name: build wait_kit
  shell: >
    make
  args:
    chdir: "{{ slob_directory }}/SLOB/wait_kit"
    creates: "{{ slob_directory }}/SLOB/mywait"

- name: set slob.conf non cdb
  replace:
    path: "{{ slob_directory }}/SLOB/slob.conf"
    regexp: "{{ item.regexp }}"
    replace: "{{ item.replace }}"
  when: pluggable_database == "N" 
  with_items:
    - { regexp: '^UPDATE_PCT=.*',               replace: 'UPDATE_PCT={{ slob_update_pct }}'                             }
    - { regexp: '^SCALE=.*',                    replace: 'SCALE={{ slob_scale }}'                                       }
    - { regexp: '^DATABASE_STATISTICS_TYPE=.*', replace: 'DATABASE_STATISTICS_TYPE={{ slob_database_statistics_type }}' }
    - { regexp: '.*DBA_PRIV_USER="system"',     replace: 'DBA_PRIV_USER="system"'                                       }
    - { regexp: '.*SYSDBA_PASSWD=.*',           replace: 'SYSDBA_PASSWD="{{ global_password }}"'                        }

- name: set slob.conf pdb
  replace:
    path: "{{ slob_directory }}/SLOB/slob.conf"
    regexp: "{{ item.regexp }}"
    replace: "{{ item.replace }}"
  when: pluggable_database == "Y" 
  with_items:
    - { regexp: '^UPDATE_PCT=.*',               replace: 'UPDATE_PCT={{ slob_update_pct }}'                             }
    - { regexp: '^SCALE=.*',                    replace: 'SCALE={{ slob_scale }}'                                       }
    - { regexp: '^DATABASE_STATISTICS_TYPE=.*', replace: 'DATABASE_STATISTICS_TYPE={{ slob_database_statistics_type }}' }
    - { regexp: '.*DBA_PRIV_USER="system"',     replace: 'DBA_PRIV_USER="system"'                                       }
    - { regexp: '.*SYSDBA_PASSWD=.*',           replace: 'SYSDBA_PASSWD="{{ global_password }}"'                        }
    - { regexp: '.*ADMIN_SQLNET_SERVICE=.*',    replace: 'ADMIN_SQLNET_SERVICE=//localhost/{{ database_name }}_pdb'     }
    - { regexp: '.*SQLNET_SERVICE_BASE=.*',     replace: 'SQLNET_SERVICE_BASE=//localhost/{{ database_name }}_pdb'      }

- name: change runit.sh to include snapshot numbers for awr report
  replace:
    path: "{{ slob_directory }}/SLOB/runit.sh"
    regexp: "{{ item.regexp }}"
    replace: "{{ item.replace }}"
  with_items:
    - { regexp: "define  report_name  = 'statspack.txt';", replace: "define  report_name  = 'statspack.$begin_snap-$end_snap.txt';" }
    - { regexp: "define  report_name  = 'awr.txt';"      , replace: "define  report_name  = 'awr.$begin_snap-$end_snap.txt';"       }

- name: list slob tablespace from dba_tablespaces non cdb
  shell: |
    echo "select tablespace_name from dba_tablespaces where tablespace_name = 'SLOB';
         " | {{ oracle_home }}/bin/sqlplus / as sysdba
  environment:
    ORACLE_HOME: "{{ oracle_home }}"
    ORACLE_SID: "{{ database_name }}"
  when: pluggable_database == "N"
  register: slob_tablespace_query_non_cdb
  changed_when: "'no rows selected' in slob_tablespace_query_non_cdb.stdout"
  notify: create slob tablespace non cdb

- name: list slob tablespace from dba_tablespaces pdb
  shell: |
    echo "select tablespace_name from dba_tablespaces where tablespace_name = 'SLOB';
         " | {{ oracle_home }}/bin/sqlplus sys/{{ global_password }}@//localhost/{{ database_name }}_pdb as sysdba
  environment:
    ORACLE_HOME: "{{ oracle_home }}"
    ORACLE_SID: "{{ database_name }}"
  when: pluggable_database == "Y"
  register: slob_tablespace_query_pdb
  changed_when: "'no rows selected' in slob_tablespace_query_pdb.stdout"
  notify: create slob tablespace pdb

- name: flush handlers to trigger tablespace creation
  meta: flush_handlers

- name: run setup.sh to setup oracle schemas
  shell: >
    {{ slob_directory }}/SLOB/setup.sh slob {{ slob_number_of_schemas }}
  args:
    chdir: "{{ slob_directory }}/SLOB"
  environment:
    ORACLE_HOME: "{{ oracle_home }}"
    ORACLE_SID: "{{ database_name }}"
    PATH: "{{ ansible_env.PATH }}:{{ oracle_home }}/bin"

- name: re-set slob.conf to make admin actions in runit.sh happen in the CDB
  replace:
    path: "{{ slob_directory }}/SLOB/slob.conf"
    regexp: "{{ item.regexp }}"
    replace: "{{ item.replace }}"
  when: pluggable_database == "Y" 
  with_items:
    - { regexp: '.*ADMIN_SQLNET_SERVICE=.*',    replace: 'ADMIN_SQLNET_SERVICE=//localhost/{{ database_name }}'     }
